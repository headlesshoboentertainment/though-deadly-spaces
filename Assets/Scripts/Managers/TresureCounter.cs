﻿using UnityEngine;
using System.Collections;

public class TresureCounter : MonoBehaviour {

    public static TresureCounter TheTresureCounter { get; set; }

    void Awake()
    {
        if (TheTresureCounter == null)
        {
            //If I am the first instance, make me the Singleton
            TheTresureCounter = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != TheTresureCounter)
                Destroy(this.gameObject);
        }
    }

    public int cPotionsTaken { get; set; }
    public int cSpeedBoostsTaken { get; set; }
    public int cAttackSpeedBoostsTaken { get; set; }
    public int cDamageBoostsTaken { get; set; }
    public int cPotionsLeft { get; set; }

    private TreasureSpawner mTreasureSpawner;

	// Use this for initialization
	void Start () {
        Init();
	}

    void Init()
    {
        mTreasureSpawner = GameObject.Find("Managers").GetComponent<TreasureSpawner>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void AddPotions(int pPotions)
    {
        cPotionsLeft += pPotions;
    }

    public void SpawnPotions()
    {
        mTreasureSpawner.SpawnPotions(cPotionsLeft);
    }

    public void Tresure(BoostType pBoostType)
    {
        switch (pBoostType)
        {
            case BoostType.AttackSpeed :
                cAttackSpeedBoostsTaken++;
                break;

            case BoostType.Damage:
                cDamageBoostsTaken++;
                break;

            case BoostType.Health:
                cPotionsTaken++;
                cPotionsLeft--;
                break;

            case BoostType.Speed:
                cSpeedBoostsTaken++;
                break;
        }

    }
}
