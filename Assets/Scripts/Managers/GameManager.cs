﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class Game
{
    public int Level;
    public int Potions;
    public int Treasures;
    public BossAttributes TheBossAttributes;
}

[System.Serializable]
public class BossAttributes
{
    public int Damage;   
    public int Health;

    public float Speed;
    public float AttackSpeed;
}

public class GameManager : MonoBehaviour {

    

    public Game[] TheGameMaker;

    public int Level;

    private int mPreviousLevel;

    private TreasureSpawner mTresureSpawner;
    private TresureCounter mTreasureCounter;
    private SpawnBoss mSpawnBoss;

    private Game mCurrentGame;

    public static GameManager TheGameManager { get; set; }

    private bool mHasJustRestarted;

    private Transform mPlayer;

    void Awake()
    {
        if (TheGameManager == null)
        {
            //If I am the first instance, make me the Singleton
            TheGameManager = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != TheGameManager)
            {
                Destroy(this.gameObject);
            }
        }

       
    }

	// Use this for initialization
	void Start () {
        Init();
	}

    void Init()
    {
        mTresureSpawner = GameObject.Find("Managers").GetComponent<TreasureSpawner>();
        mTreasureCounter = GetComponent<TresureCounter>();

        
        mSpawnBoss = GameObject.Find("Managers").GetComponent<SpawnBoss>();
        DestroyWrongPlayer();
        mPlayer = GameObject.Find("ThePlayer").transform;
        SetPlayerReset();
        mCurrentGame = GetCurrentGame();
        StartingNewLevel();
        mPlayer.GetComponent<PlayerMovement>().cCanMove = true;
    }

    void WriteToLevelText()
    {
        GameObject.Find("LevelText").GetComponent<Text>().text = "Level : "+Level;
    }

    void DestroyWrongPlayer()
    {
        foreach(GameObject tPlayer in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (tPlayer.name != "ThePlayer")
            {
                Destroy(tPlayer);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
        mCurrentGame = GetCurrentGame();
        WriteToLevelText();
	}

    void SetPlayerReset()
    {
        PlayerReset tPlayerReset = mPlayer.GetComponent<PlayerReset>();
        tPlayerReset.Reset();
    }



    public void GoingToTreasureRoom()
    {
        AddTreasures();
        SpawnTreasures();
    }

    void AddTreasures()
    {
        mTreasureCounter.AddPotions(mCurrentGame.Potions);
    }

    void SpawnTreasures()
    {
        mTresureSpawner.SpawnObject(mCurrentGame.Treasures);
        mTreasureCounter.SpawnPotions();
    }

    Game GetCurrentGame()
    {
        Game tCurrentGame = new Game();

        tCurrentGame = TheGameMaker[Level - 1];

        return tCurrentGame;
    }

    public void GoingToNextLevel()
    {
        LevelUp();
        StartCoroutine(Restart());       
    }

    public IEnumerator Restart()
    {
        mPlayer.GetComponent<PlayerMovement>().cCanMove = false;
        mHasJustRestarted = true;        
        Application.LoadLevel(Application.loadedLevel);
        yield return new WaitForSeconds(1);
        SendMessageUpwards("Init", SendMessageOptions.DontRequireReceiver);
    }

    public IEnumerator RestartReset()
    {

        Level = 1;
        Application.LoadLevel(Application.loadedLevel);
        yield return new WaitForSeconds(1);
        SendMessageUpwards("Init", SendMessageOptions.DontRequireReceiver);
    }

    public void StartingNewLevel()
    {
        
        mSpawnBoss.SpawnObject(1, mCurrentGame.TheBossAttributes);
    }

    void LevelUp()
    {
        Level++;
    }
}
