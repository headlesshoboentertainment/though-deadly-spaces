﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

   

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	if(!IsMusicPlaying())
    {
        PlayMusic();
    }
	}

    void PlayMusic()
    {
        int tRandomNumber = Random.Range(0, 2);

         Transform tTransformToPlayOn =  transform.GetChild(tRandomNumber);
         tTransformToPlayOn.GetComponent<AudioSource>().Play();

    }

    bool IsMusicPlaying()
    {
        bool tIsMusicPlaying = false;

        foreach (Transform tChild in transform)
        {
            AudioSource tAudioSource = tChild.GetComponent<AudioSource>();
            if(tAudioSource.isPlaying)
            {
                tIsMusicPlaying = true;
            }
        }

        return tIsMusicPlaying;
    }
}
