﻿using UnityEngine;
using System.Collections;

public class TreasureSpawner : MonoBehaviour {

    public GameObject[] ObjectToSpawns;
    public GameObject Potions;

    public Vector2 MinOffset;
    public Vector2 MaxOffset;

    public Transform[] PlacesToSpawnAt;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    Vector3 CalculateOffSet()
    {
        Vector3 tOffset = Vector2.zero;

        tOffset = new Vector3(Random.Range(MinOffset.x, MaxOffset.x), Random.Range(MinOffset.y, MaxOffset.y));

        return tOffset;
    }

    public void SpawnObject(int pNumberOfObjectToSpawn)
    {
        for(int tSpawnNumber = 0; tSpawnNumber < pNumberOfObjectToSpawn;tSpawnNumber++)
        {
            Spawn(GetRandomObject(), GetRandomPlaceToSpawn().position + CalculateOffSet());
        }
    }

    public void SpawnPotions(int pNumberOfObjectToSpawn)
    {
        for (int tSpawnNumber = 0; tSpawnNumber < pNumberOfObjectToSpawn; tSpawnNumber++)
        {
            Spawn(Potions, GetRandomPlaceToSpawn().position + CalculateOffSet());
        }
    }

    void Spawn(GameObject pObject,Vector2 pPosition)
    {
        Instantiate(pObject, pPosition, pObject.transform.rotation);
    }


    Transform GetRandomPlaceToSpawn()
    {
        Transform tRandomObject = null;

        int tRandomNumber = Random.Range(0, PlacesToSpawnAt.Length);
        tRandomObject =  PlacesToSpawnAt[tRandomNumber];

        return tRandomObject;
    }

    GameObject GetRandomObject()
    {
        GameObject tRandomObject = null;

        int tRandomNumber = Random.Range(0, ObjectToSpawns.Length);
        tRandomObject = ObjectToSpawns[tRandomNumber];

        return tRandomObject;
    }
}
