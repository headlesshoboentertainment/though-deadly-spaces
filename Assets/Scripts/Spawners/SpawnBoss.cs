﻿using UnityEngine;
using System.Collections;

public class SpawnBoss : MonoBehaviour {

    public GameObject[] Bosses;

    public Transform[] PlacesToSpawn;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SpawnObject(int pNumberOfObjectToSpawn, BossAttributes pBossAttributes)
    {
        for (int tSpawnNumber = 0; tSpawnNumber < pNumberOfObjectToSpawn; tSpawnNumber++)
        {
            Spawn(GetRandomObject(), GetRandomPlaceToSpawn().position,pBossAttributes);
        }
    }



    void Spawn(GameObject pObject, Vector2 pPosition, BossAttributes pBossAttributes)
    {
        GameObject tObjectBoss =  Instantiate(pObject, pPosition, pObject.transform.rotation) as GameObject;
        Boss tBoss = tObjectBoss.GetComponent<Boss>();
        AddSpecsToBoss(tBoss, pBossAttributes);
       
    }

    void AddSpecsToBoss(Boss pBoss, BossAttributes pBossAttributes)
    {
        pBoss.Speed = pBossAttributes.Speed;
        pBoss.AttackSpeed = pBossAttributes.AttackSpeed;
        pBoss.Damage = pBossAttributes.Damage;
        pBoss.Health = pBossAttributes.Health;
    }


    Transform GetRandomPlaceToSpawn()
    {
        Transform tRandomObject = null;

        int tRandomNumber = Random.Range(0, PlacesToSpawn.Length);
        tRandomObject = PlacesToSpawn[tRandomNumber];

        return tRandomObject;
    }

    GameObject GetRandomObject()
    {
        GameObject tRandomObject = null;

        int tRandomNumber = Random.Range(0, Bosses.Length);
        tRandomObject = Bosses[tRandomNumber];

        return tRandomObject;
    }
}
