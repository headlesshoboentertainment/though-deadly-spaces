﻿using UnityEngine;
using System.Collections;

public enum BoostType
{
    AttackSpeed,
    Damage,
    Health,
    Speed
}

public class Boost : MonoBehaviour {

    public BoostType TheBoostType;

    private PlayerMovement mPlayerMovement;
    private PlayerHit mPlayerHit;
    private PlayerHealth mPlayerHealth;
    private TresureCounter mTresureCounter;

    private Transform mPlayer;

	// Use this for initialization
	void Start () {
        mPlayer = GameObject.FindGameObjectWithTag("Player").transform;

        mPlayerHealth = mPlayer.GetComponent<PlayerHealth>();
        mPlayerHit = mPlayer.GetComponent<PlayerHit>();
        mPlayerMovement = mPlayer.GetComponent<PlayerMovement>();
        mTresureCounter = GameObject.Find("GameManager").GetComponent<TresureCounter>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void BoostThePlayer(float tAmount)
    {
        switch(TheBoostType)
        {
            case BoostType.AttackSpeed :
                mPlayerHit.AttackSpeedBuff(tAmount);
                break;

            case BoostType.Damage:
                mPlayerHit.AddDamage((int)tAmount);
                break;

            case BoostType.Health:
                mPlayerHealth.AddHealth((int)tAmount);
                break;

            case BoostType.Speed:
                mPlayerMovement.AddToSpeed(tAmount);
                break;
        }

        mTresureCounter.Tresure(TheBoostType);
    }
}
