﻿using UnityEngine;
using System.Collections;
using XboxCtrlrInput;

public class Reload : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        CheckInput();
	}

    void CheckInput()
    {
        if(XCI.GetButton(XboxButton.Back))
        {
            Destroy(GameObject.Find("GameManager"));
            Destroy(GameObject.Find("ThePlayer"));
            Application.LoadLevel(Application.loadedLevel);
        }
    }
}
