﻿using UnityEngine;
using System.Collections;
using XboxCtrlrInput;

public class Fart : MonoBehaviour {

    private Animator mAnimator;

	// Use this for initialization
	void Start () {
        mAnimator = GameObject.Find("ThePlayer").GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        CheckInput();
	}

    void CheckInput()
    { 
        if(XCI.GetButton(XboxButton.Y))
        {
            DoFart();
            StartCoroutine(PlayAnimation());
        }
    }

    IEnumerator PlayAnimation()
    {
        mAnimator.SetBool("Prut",true);
        yield return new WaitForSeconds(0.75f);
        mAnimator.SetBool("Prut", false);
    }

    void DoFart()
    {
        int tRandomNumber = Random.Range(0, 3);
        transform.GetChild(tRandomNumber).GetComponent<AudioSource>().Play();
    }
}

