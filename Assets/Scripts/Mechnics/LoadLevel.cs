﻿using UnityEngine;
using System.Collections;

public class LoadLevel : MonoBehaviour {

    public string LevelToLoad;
    public float Time;

	// Use this for initialization
	void Start () {
        Invoke("LoadTheLevel", Time);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void LoadTheLevel()
    {
        Application.LoadLevel(LevelToLoad);
    }
}
