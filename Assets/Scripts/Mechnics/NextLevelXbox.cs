﻿using UnityEngine;
using System.Collections;
using XboxCtrlrInput;

public class NextLevelXbox : MonoBehaviour {

    public string LevelToLoad;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        CheckInput();
	}

    void CheckInput()
    {
        if(XCI.GetButton(XboxButton.A))
        {
            StartTheGame();
        }
        if (XCI.GetButton(XboxButton.B))
        {
            Exit();
        }
    }

    void StartTheGame()
    {
        GetComponent<AudioSource>().Play();
        Application.LoadLevel(LevelToLoad);
    }

    void Exit()
    {
        Application.Quit();
    }
}
