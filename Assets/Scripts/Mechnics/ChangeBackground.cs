﻿using UnityEngine;
using System.Collections;

public class ChangeBackground : MonoBehaviour {

    public Sprite TheChangeBackground;

    private Sprite mStartSprite;

	// Use this for initialization
	void Start () {
        mStartSprite = GetComponent<SpriteRenderer>().sprite;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Reset()
    {
        GetComponent<SpriteRenderer>().sprite = mStartSprite;
    }

    public void BackgroundChange()
    {
        GetComponent<SpriteRenderer>().sprite = TheChangeBackground;
    }
}
