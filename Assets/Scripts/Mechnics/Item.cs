﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {

    public float Amount;
    

	// Use this for initialization
	void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D pCollider)
    {
        if(pCollider.tag == "Player")
        {
            GetComponent<AudioSource>().Play();
            SendMessage("BoostThePlayer", Amount);
            Destroy(gameObject);
        }
        
    }
}
