﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

    public float TweenTime;

    public string MethodToCallOnGameManager;

    public Vector3 Offset;

    public Transform OtherDoor;

    private GameManager mGameManager;

    public bool cLocked { get; set; }

	// Use this for initialization
	void Start () {
        mGameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        Invoke("Lock", 0.25f);
	}
	
	// Update is called once per frame
	void Update () {
        IsUnlocked();
	}

    void Lock()
    {
        cLocked = true;
    }

    public void Unlock()
    {
        cLocked = false;
    }

    void IsUnlocked()
    {
        if (!cLocked)
        {
            GameObject.Find("BossBackground1").SendMessage("BackgroundChange");
            GameObject.Find("BossBackground2").SendMessage("BackgroundChange");
        }
        else
        {
            GameObject.Find("BossBackground1").SendMessage("Reset");
            GameObject.Find("BossBackground2").SendMessage("Reset");
        }
    }

    void OnTriggerEnter2D(Collider2D pCollider)
    {
        if(pCollider.tag == "Player" && !cLocked)
        {
            mGameManager.SendMessage(MethodToCallOnGameManager);
            SetPlayerAtOtherDoor(pCollider.transform);
            iTween.MoveBy(Camera.main.gameObject, new Vector3(0,FindHeightOfCamera(),0), TweenTime);
        }
    }

    void SetPlayerAtOtherDoor(Transform pPlayer)
    {
        pPlayer.position = OtherDoor.position + Offset;
    }

    float FindHeightOfCamera()
    {
        float tHeightOfCamera = 0;

        tHeightOfCamera = 2f *  Camera.main.orthographicSize;
        
        return tHeightOfCamera;
    }

}
