﻿using UnityEngine;
using System.Collections;
using XboxCtrlrInput;

public class PlayerShield : MonoBehaviour {

    public float DurationOfShield;
    public float Cooldown;

    private PlayerHealth mPlayerHealth;
    private PlayerMovement mPlayerMovement;

    private float mNextTimeToCanBeUsed;

    private Animator mAnimator;

	// Use this for initialization
	void Start () {
        mAnimator = GetComponent<Animator>();
        mPlayerMovement = GetComponent<PlayerMovement>();
        mPlayerHealth = GetComponent<PlayerHealth>();
	}
	
	// Update is called once per frame
	void Update () {
        CheckInput();
	}

    void CheckInput()
    {     

        if (XCI.GetButton(XboxButton.B, mPlayerMovement.ControllerNumber) && !IsOnCooldown())
        {
            StartCoroutine(PlayAnimation());
            StartCoroutine(StartShield());
           
            mNextTimeToCanBeUsed =  CalculateNextTimeCanBeUsed();
        }
    }

    IEnumerator StartShield()
    {
        mPlayerHealth.cIsInvinsible = true;
        Transform tChild = transform.FindChild("ShieldOnPlayer");
        tChild.GetComponent<SpriteRenderer>().enabled = true;
        tChild.GetComponent<Animator>().SetBool("Shield", true);
        yield return new WaitForSeconds(DurationOfShield);
        tChild.GetComponent<SpriteRenderer>().enabled = false;
        tChild.GetComponent<Animator>().SetBool("Shield", false);
        mPlayerHealth.cIsInvinsible = false;
    }

  

    
    IEnumerator PlayAnimation()
    {
        mAnimator.SetBool("Shield", true);
        yield return new WaitForSeconds(0.55f);
        mAnimator.SetBool("Shield", false);
    }


    bool IsOnCooldown()
    {
        bool tIsOnCooldown = true;

        if(mNextTimeToCanBeUsed < Time.time)
        {
            tIsOnCooldown = false;
        }

        return tIsOnCooldown;
    }

    float CalculateNextTimeCanBeUsed()
    {
        float tNextTimeCanBeUsed = 0;

        tNextTimeCanBeUsed = Time.time + Cooldown;

        return tNextTimeCanBeUsed;
    }
}
