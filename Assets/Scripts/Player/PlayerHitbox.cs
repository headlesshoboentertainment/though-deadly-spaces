﻿using UnityEngine;
using System.Collections;



public class PlayerHitbox : MonoBehaviour {

    public bool cIsEnemiesInHitbox { get; set; }

    private GameObject mEnemy;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void AttackEnemy(int pDamage)
    {
        if(mEnemy != null)
        {
            GetComponent<AudioSource>().Play();
            mEnemy.SendMessage("TakeDamage", pDamage);
        }
    }
    

    void OnTriggerEnter2D(Collider2D pCollider)
    {
        if(pCollider.tag == "Enemy")
        {
            cIsEnemiesInHitbox = true;
            mEnemy = pCollider.gameObject;
        }
    }

    void OnTriggerExit2D(Collider2D pCollider)
    {
        if (pCollider.tag == "Enemy")
        {
            mEnemy = null;
            cIsEnemiesInHitbox = false;
        }
    }


}
