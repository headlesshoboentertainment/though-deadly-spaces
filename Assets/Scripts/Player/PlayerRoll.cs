﻿using UnityEngine;
using System.Collections;
using XboxCtrlrInput;

public class PlayerRoll : MonoBehaviour {

    public float Speed;
    public float LengthToRoll;
    public float DestinationReachCheck;
    public float Cooldown;
    public float StartDistanceCheck;

    private Vector3[] mPlayerRollDirection = new[] 
    {         
        new Vector3(0f, 1f), // UP
        new Vector3(0, -1f), // DOWN
        new Vector3(-1f, 0f), // LEFT
        new Vector3(1f, 0f), // RIGHT       
    };
      

    private PlayerMovement mPlayerMovement;

    private bool mRoll;
    private bool mCheckIfCanStart;

    private Vector2 mDestination;

    private Vector3 mRollDirection;

    private Transform mMyTransform;

    private Vector2 mMoveDirection;

    private PlayerCollision mPlayerCollision;

    private Animator mAnimator;

    private float mNextTimeToCanBeUsed;

	// Use this for initialization
	void Start () {
        Init();
	}

    void Init()
    {
        mPlayerMovement = GetComponent<PlayerMovement>();
        mPlayerCollision = GetComponent<PlayerCollision>();
        mAnimator = GetComponent<Animator>();
        mMyTransform = transform;
        mMoveDirection = Vector2.zero;
        mRoll = false;
    }
	
	// Update is called once per frame
	void Update () {
                
	    CheckInput();
        ShouldStop();
        if(mRoll)
        {
            Move();
            CheckIfReachedDestination();            
        }

        if (mCheckIfCanStart)
        {
            CanStartWithoutCollision();
            mCheckIfCanStart = false;
        }
	}

    void ShouldStop()
    {
        if(mPlayerCollision.CollindingWithAnything(mMoveDirection))
        {
            Disable();
        }
    }

    bool IsOnCooldown()
    {
        bool tIsOnCooldown = true;

        if (mNextTimeToCanBeUsed < Time.time)
        {
            tIsOnCooldown = false;
        }

        return tIsOnCooldown;
    }

    float CalculateNextTimeCanBeUsed()
    {
        float tNextTimeCanBeUsed = 0;

        tNextTimeCanBeUsed = Time.time + Cooldown;

        return tNextTimeCanBeUsed;
    }

    void Move()
    {
        mMoveDirection = mRollDirection * Speed * Time.deltaTime;

        mMyTransform.position = new Vector3(mMyTransform.position.x + mMoveDirection.x, mMyTransform.position.y + mMoveDirection.y, mMyTransform.position.z);
    }

    void CanStartWithoutCollision()
    {
        if(mPlayerCollision.CollindingWithAnything(mMoveDirection,StartDistanceCheck))
        {
            MoveBack();
            Disable();
        }
    }

    void MoveBack()
    {
        mMyTransform.position = new Vector3(mMyTransform.position.x - mMoveDirection.x, mMyTransform.position.y - mMoveDirection.y, mMyTransform.position.z);
    }

    void CheckInput()
    {
        int tDPADPressed = DPADPressed();

        if (XCI.GetButton(XboxButton.X, mPlayerMovement.ControllerNumber) && tDPADPressed != int.MinValue && !IsOnCooldown())
        {
            Roll(tDPADPressed);
            mNextTimeToCanBeUsed = CalculateNextTimeCanBeUsed();
        }
    }

    void Roll(int pDirection)
    {
        Enable(pDirection);
      StartCoroutine(PlayAnimation());
    }

    void SetPlayerMovementStuff(int pDirection)
    {
        mPlayerMovement.cCanMove = false;
        mPlayerMovement.cFacingDirection = (Direction)pDirection;
    }


    IEnumerator PlayAnimation()
    {
        mAnimator.SetBool("Dash", true);
        yield return new WaitForSeconds(0.6f);
        mAnimator.SetBool("Dash", false);

    }

    void CheckIfReachedDestination()
    {
        if(HasReachedDestination())
        {
            Disable();
        }
    }

    void Enable(int pDirection)
    {
        SetPlayerMovementStuff(pDirection);
        mRollDirection = mPlayerRollDirection[pDirection];
        mDestination = CalculateDestination(pDirection);
        mRoll = true;
        mCheckIfCanStart = true;
    }

    void Disable()
    {
        mRoll = false;
        mPlayerMovement.cCanMove = true;
    }

    bool HasReachedDestination()
    {
        bool tReachedDestination = false;

        if (Vector2.Distance(mMyTransform.position, mDestination) < DestinationReachCheck)
        {
            tReachedDestination = true;
        }

        return tReachedDestination;
    }

    Vector2 CalculateDestination(int pDirection)
    {
        Vector2 tDestination = Vector2.zero;

        tDestination = mMyTransform.position + (mRollDirection * LengthToRoll);

        return tDestination;
    }


    int DPADPressed()
    {
        int tDPADPressed = int.MinValue;

        for (int tDPADNumber = 2; tDPADNumber < 4; tDPADNumber++)
        {
            if (XCI.GetDPad((XboxDPad)tDPADNumber))
            {
                tDPADPressed = tDPADNumber;
            }
        }

        return tDPADPressed;
    }
}
