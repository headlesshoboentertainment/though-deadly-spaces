﻿using UnityEngine;
using System.Collections;

public class PlayerReset : MonoBehaviour {

    private Vector2 mStartPosition;

	// Use this for initialization
	void Start () {
        mStartPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Reset()
    {
        transform.position = mStartPosition;
    }
}
