﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour {

    

    public int MaxHealth;

    private int mHealth;

    private bool mHasJustRestarted;

    public bool cIsInvinsible { get; set; }

    private Transform mHealthBars;

	// Use this for initialization
	void Start () {
        Init();
	}

    void Init()
    {
        mHealth = MaxHealth;
       
    }
	
	// Update is called once per frame
	void Update () {
        mHealthBars = GameObject.Find("HealthBar").transform;
        ShouldDie();
        ShouldInit();
        SetHealthBars();
	}

    void SetHealthBars()
    {
        for (int tHealthNumber = 0; tHealthNumber < mHealthBars.childCount; tHealthNumber++)
       {
           if((tHealthNumber+1) <= mHealth)
           {
               mHealthBars.GetChild(tHealthNumber).gameObject.SetActive(true);
           }
            else
           {
               mHealthBars.GetChild(tHealthNumber).gameObject.SetActive(false);
           }
       }
    }

    void ShouldInit()
    {
        if(mHasJustRestarted)
        {
            mHasJustRestarted = false;
            SendMessageUpwards("Init", SendMessageOptions.DontRequireReceiver);
            GameObject.Find("GameManager").GetComponent<GameManager>().SendMessage("Init");
        }
    }

    void ShouldDie()
    {
        if (mHealth <= 0)
        {

            Die();
        }
    }

    void Die()
    {
        mHasJustRestarted = true;
        Application.LoadLevel("Start");
        Destroy(GameObject.Find("GameManager"));
        Destroy(gameObject);
    }

    

    void TakeDamage(int pDamage)
    {
        if (!cIsInvinsible)
        {
            mHealth -= pDamage;
        }
    }

    public void AddHealth(int pHealth)
    {
        if ((pHealth + mHealth) <= MaxHealth)
        {
            mHealth += pHealth;
        }
    }
}
