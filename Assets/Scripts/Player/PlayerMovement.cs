﻿using UnityEngine;
using System.Collections;
using XboxCtrlrInput;
using UnityEngine.UI;

public enum Direction
{
    Up,
    Down,
    Left,
    Right
}

public enum AnimationsState
{
    Idle,
    Run
}

public class PlayerMovement : MonoBehaviour {

    public static PlayerMovement ThePlayerMovement { get; set; }

    void Awake()
    {
        if (ThePlayerMovement == null)
        {
            //If I am the first instance, make me the Singleton
            ThePlayerMovement = this;
            
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != ThePlayerMovement)
                Destroy(this.gameObject);
        }
    }

    public float Speed;

    public int ControllerNumber;

    public Direction cFacingDirection { get; set; }

    public int[] FacingRotations;

    private float mSpeed;

    private Transform mMyTransform;


    private Vector2 mMoveDirection;

    private PlayerCollision mPlayerCollision;

    private PlayerHealth mPlayerHealth;

    private Vector3 mStartScale;

    public bool cCanMove { get; set; }

    private bool mIsNoCollision;

    private Animator mAnimator;

    private AnimationsState mTheAnimationsState;
    private AnimationsState mPreviousAnimationsState;

	// Use this for initialization
	void Start () {
        Init();
	}

    void WriteToText()
    {
        GameObject.Find("SPE").GetComponent<Text>().text = ((int)mSpeed).ToString();
    }

    void Init()
    {
        mAnimator = GetComponent<Animator>();
        mStartScale = transform.lossyScale;
        cCanMove = true;
        mSpeed = Speed;
        mMyTransform = transform;
        cFacingDirection = Direction.Right;
        mPlayerCollision = GetComponent<PlayerCollision>();
    }

	
	// Update is called once per frame
	void Update () {
        SetInputFromController();
        ShouldStop();
        WriteToText();
        if (cCanMove && mIsNoCollision || IsStuck())
        {
       
        Move();
        SetState();
        
        }

        PlayAnimation();
        SetFacingDirection();
        SetPlayerFacing();
       
	}

    void PlayAnimation()
    {
        if(mTheAnimationsState == AnimationsState.Run && HasAnimationChanged())
        {
            mAnimator.SetBool("Run", true);
            mAnimator.SetBool("Idle", false);
        }
        else if (mTheAnimationsState == AnimationsState.Idle && HasAnimationChanged())
        {
            mAnimator.SetBool("Idle", true);
            mAnimator.SetBool("Run", false);
        }
        
    }

    bool HasAnimationChanged()
    {
        bool tAnimationChanged = false;

        if(mTheAnimationsState != mPreviousAnimationsState)
        {
            tAnimationChanged = true;
        }

        mPreviousAnimationsState = mTheAnimationsState;

        return tAnimationChanged;
    }

    public void AddToSpeed(float pSpeed)
    {
        mSpeed += pSpeed;
    }

    void ShouldStop()
    {
        if(mPlayerCollision.CollindingWithAnything(mMoveDirection))
        {
            mIsNoCollision = false;
        }
        
    }

    bool IsStuck()
    {
        bool tIsStuck = false;

        mMyTransform.position = new Vector3(mMyTransform.position.x + mMoveDirection.x, mMyTransform.position.y + mMoveDirection.y, mMyTransform.position.z);

        if (!mPlayerCollision.CollindingWithAnything(mMoveDirection))
        {
            tIsStuck = true;
        }
        else
        {
            mMyTransform.position = new Vector3(mMyTransform.position.x - mMoveDirection.x, mMyTransform.position.y - mMoveDirection.y, mMyTransform.position.z);
        }

        return tIsStuck;
    }

    void SetInputFromController()
    {
        mMoveDirection = new Vector2(XCI.GetAxis(XboxAxis.LeftStickX, ControllerNumber), XCI.GetAxis(XboxAxis.LeftStickY, ControllerNumber));

        mMoveDirection *= (mSpeed * Time.deltaTime);
        mIsNoCollision = true;
    }

    void Move()
    {      
        mMyTransform.position = new Vector3(mMyTransform.position.x + mMoveDirection.x, mMyTransform.position.y + mMoveDirection.y, mMyTransform.position.z);
    }

    void SetFacingDirection()
    {
        if (Mathf.Abs(mMoveDirection.x) > 0 && Mathf.Abs(mMoveDirection.x) > Mathf.Abs(mMoveDirection.y))
        {
            if(mMoveDirection.x > 0)
            {
                cFacingDirection = Direction.Right;
            }
            if (mMoveDirection.x < 0)
            {
                cFacingDirection = Direction.Left;
            }
        }
        else if (Mathf.Abs(mMoveDirection.y) > 0 && Mathf.Abs(mMoveDirection.y) > Mathf.Abs(mMoveDirection.x))
        {
            if (mMoveDirection.y > 0)
            {
                cFacingDirection = Direction.Up;
            }
            if (mMoveDirection.y < 0)
            {
                cFacingDirection = Direction.Down;
            }
        }
    }

    void SetState()
    {
        if(mMoveDirection.x == 0 && mMoveDirection.y == 0)
        {
            mTheAnimationsState = AnimationsState.Idle;
        }
        else if (mMoveDirection.x != 0 || mMoveDirection.y != 0)
        {
            mTheAnimationsState = AnimationsState.Run;
        }
    }

    public void AttackDirection(int pDirection)
    {
       switch((Direction)pDirection)
       {
           case Direction.Left :
               if(cFacingDirection == Direction.Right)
               {
                   cFacingDirection = Direction.Left;
               }
               break;

           case Direction.Right:
               if (cFacingDirection == Direction.Left)
               {
                   cFacingDirection = Direction.Right;
               }
               break;

           case Direction.Up:
               if (cFacingDirection == Direction.Down)
               {
                   cFacingDirection = Direction.Up;
               }
               break;

           case Direction.Down:
               if (cFacingDirection == Direction.Up)
               {
                   cFacingDirection = Direction.Down;
               }
               break;
       }
    }

    void SetPlayerFacing()
    {
        if(cFacingDirection == Direction.Left)
        {
            transform.localScale = new Vector3(mStartScale.x, transform.lossyScale.y, transform.lossyScale.z);
        }
        else if (cFacingDirection == Direction.Right)
        {
            transform.localScale = new Vector3(-mStartScale.x, transform.lossyScale.y, transform.lossyScale.z);
        }
    }
}
