﻿using UnityEngine;
using System.Collections;

public class PlayerCollision : MonoBehaviour {

    public LayerMask LayerToHit;

    public float DistanceToCheck;

    private Vector3[] mCollisionRaycastPositions = new[] 
    {         
        new Vector3(0.49f, 0.49f), // 
        new Vector3(0.49f, -0.49f), // 
        new Vector3(-0.49f, 0.49f), // 
        new Vector3(-0.49f, -0.49f), //        
    };
      

    private Transform mMyTransform;

	// Use this for initialization
	void Start () {
        mMyTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public bool CollindingWithAnything(Vector2 pDirection)
    {
        bool tCollindingWithAnything = false;

        foreach (Vector3 tRaycastPosition in mCollisionRaycastPositions)
        {
            RaycastHit2D[] tHits = Physics2D.RaycastAll(mMyTransform.position + tRaycastPosition, pDirection, DistanceToCheck, LayerToHit);

            if(HitsAnything(tHits))
            {
                tCollindingWithAnything = true;
                break;
            }
        }

        return tCollindingWithAnything;
    }

    


    public bool CollindingWithAnything(Vector2 pDirection,float pExtraDistanceToCheckFor)
    {
        bool tCollindingWithAnything = false;

        foreach (Vector3 tRaycastPosition in mCollisionRaycastPositions)
        {
            RaycastHit2D[] tHits = Physics2D.RaycastAll(mMyTransform.position + tRaycastPosition, pDirection, DistanceToCheck + pExtraDistanceToCheckFor, LayerToHit);

            if (HitsAnything(tHits))
            {
                tCollindingWithAnything = true;
                break;
            }
        }

        return tCollindingWithAnything;
    }


    bool HitsAnything(RaycastHit2D[] pHits)
    {
        bool tHitsAnything = false;

        foreach(RaycastHit2D tHit in pHits)
        {
            if(tHit.transform != mMyTransform)
            {
                tHitsAnything = true;
            }
        }

        return tHitsAnything;
    }
}
