﻿using UnityEngine;
using System.Collections;
using XboxCtrlrInput;
using UnityEngine.UI;

public class PlayerHit : MonoBehaviour {

    public PlayerHitbox[] PlayerHitboxes;

    public int Damage;

    public float AttackSpeed;

    private PlayerMovement mPlayerMovement;

    private float mNextTimeToAttack;

    private int mDamage;
    private float mAttackSpeed;

    private Animator mAnimator;
    
	// Use this for initialization
	void Start () 
    {
        Init();
	}

    void Init()
    {
        mDamage = Damage;
        mAttackSpeed = AttackSpeed;
        mPlayerMovement = GetComponent<PlayerMovement>();
        mAnimator = GetComponent<Animator>();
    }
    void WriteToText()
    {
        GameObject.Find("DEX").GetComponent<Text>().text = Mathf.Ceil(mAttackSpeed).ToString();
        GameObject.Find("STR").GetComponent<Text>().text = mDamage.ToString();
    }

	
	// Update is called once per frame
	void Update () {
        CheckInput();
        WriteToText();
	}

    void ChooseRandomSound()
    {
        int tRandomNumber = Random.Range(0, 2);
        if(tRandomNumber == 0)
        {
            transform.FindChild("SwordSwipe1").GetComponent<AudioSource>().Play();
        }
        else if (tRandomNumber == 1)
        {
            transform.FindChild("SwordSwipe2").GetComponent<AudioSource>().Play();
        }
    }

    void CheckInput()
    {
        int tDPADPressed = DPADPressed();

        if (XCI.GetButton(XboxButton.A, mPlayerMovement.ControllerNumber) && tDPADPressed != int.MinValue && CanAttack())
        {
            ChooseRandomSound();
            StartCoroutine(PlayAnimation());
            AttackEnemy(tDPADPressed);
            mNextTimeToAttack = CalculateNextTimeToAttack();
        }
        else if(XCI.GetButton(XboxButton.A, mPlayerMovement.ControllerNumber)  && CanAttack())
        {
            ChooseRandomSound();
            StartCoroutine(PlayAnimation());
            AttackEnemy((int)mPlayerMovement.cFacingDirection);
            mNextTimeToAttack = CalculateNextTimeToAttack();
        }
    }

    IEnumerator PlayAnimation()
    {
        mAnimator.SetBool("Slaa", true);
        yield return new WaitForSeconds(0.65f);
        mAnimator.SetBool("Slaa", false);

    }

    public void AddDamage(int pDamage)
    {
        mDamage += pDamage;
    }

    public void AttackSpeedBuff(float pAttackSpeed)
    {
        mAttackSpeed -= pAttackSpeed;
    }

    int DPADPressed()
    {
        int tDPADPressed = int.MinValue;

        for (int tDPADNumber = 0; tDPADNumber < 4;tDPADNumber++)
        {            
            if(XCI.GetDPad((XboxDPad)tDPADNumber))
            {
                tDPADPressed = tDPADNumber;
            }
        }

            return tDPADPressed;
    }

    bool CanAttack()
    {
        bool tCanAttack = false;

        if(mNextTimeToAttack < Time.time)
        {
            tCanAttack = true;
        }

        return tCanAttack;
    }

    float CalculateNextTimeToAttack()
    {
        float tNextAttack = 0;

        tNextAttack = Time.time + mAttackSpeed;

        return tNextAttack;
    }

    void AttackEnemy(int pDirection)
    {
         PlayerHitbox tPlayerHitbox =  PlayerHitboxes[pDirection];
         tPlayerHitbox.AttackEnemy(mDamage);

         mPlayerMovement.AttackDirection(pDirection);
    }
}
