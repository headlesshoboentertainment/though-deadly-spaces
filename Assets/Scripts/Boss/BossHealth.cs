﻿using UnityEngine;
using System.Collections;

public class BossHealth : MonoBehaviour {

    private int mHealth;

    private Boss mBoss;

	// Use this for initialization
	void Start () {
        mBoss = GetComponent<Boss>();
        mHealth = mBoss.Health;
    }
	
	// Update is called once per frame
	void Update () {
        ShouldDie();
	}

    void ShouldDie()
    {
        if(mHealth <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        GetComponent<AudioSource>().Play();
        UnlockDoor();
        Destroy(gameObject);
    }

    void UnlockDoor()
    {
        foreach(GameObject tDoor in GameObject.FindGameObjectsWithTag("Door"))
        {
            tDoor.SendMessage("Unlock");
         
        }
    }

    void TakeDamage(int pDamage)
    {
        mHealth -= pDamage;
    }
}
