﻿using UnityEngine;
using System.Collections;

public class Boss : MonoBehaviour {
    [HideInInspector]
    public int Damage;
    [HideInInspector]
    public int Health;
    [HideInInspector]
    public float AttackSpeed;
    [HideInInspector]
    public float Speed;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
