﻿using UnityEngine;
using System.Collections;

public class BossRotate : MonoBehaviour {

    public float RotateSpeed;

    public float MarginErrorInRotation;

    public bool cRotate { get; set; }

    private float mRotateSpeed;

    private int mRounds;

    private int mRoundsToGo;

	// Use this for initialization
	void Start () {
        mRotateSpeed = RotateSpeed;
	}
	
	// Update is called once per frame
	void Update () {
        if (cRotate)
        {
            Rotate();
            Round();
            CheckIfHasTakenTheRounds();
        }
        
	}

    void Round()
    {
        if(transform.eulerAngles.z < MarginErrorInRotation && transform.eulerAngles.z < 0)
        {
            mRounds++;
        }
    }

    void CheckIfHasTakenTheRounds()
    {
        if(HasRunTheRounds())
        {
            Disable();
        }
    }

    public void Disable()
    {
        foreach(Transform tChild in transform)
        {
            if(tChild.tag == "Stone")
            {           
            Rigidbody2D tRigidbody = tChild.GetComponent<Rigidbody2D>();
            tRigidbody.isKinematic = false;
            tRigidbody.gravityScale = 1;
            }
        }
        cRotate = false;
    }

    bool HasRunTheRounds()
    {
        bool tHasRunRounds = false;

        if(mRoundsToGo != 0)
        {
            if(mRounds >= mRoundsToGo)
            {
                tHasRunRounds = true;
            }
        }

        return tHasRunRounds;

    }

    public void Rotate(int pRounds,float pSpeed)
    {
        mRotateSpeed = pSpeed;
        cRotate = true;
    }
    public void Rotate(int pRounds)
    {
        mRotateSpeed = RotateSpeed;
        cRotate = true;
    }

    void Rotate()
    {
        transform.Rotate(new Vector3(0, 0, mRotateSpeed * Time.deltaTime));
    }
}
