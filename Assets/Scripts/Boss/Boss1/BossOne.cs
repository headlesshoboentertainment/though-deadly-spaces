﻿using UnityEngine;
using System.Collections;

public enum BossAttack
{
    Throw,
    LineAttack
}

public class BossOne : MonoBehaviour {

   

    private BossThrow mBossThrow;
    private BossRotate mBossRotate;
    private BossLineAttack mBossLineAttack;
    private BossFollowPlayer mBossFollowPlayer;

	// Use this for initialization
	void Start () {
        mBossThrow = GetComponent<BossThrow>();
        mBossRotate = GetComponent<BossRotate>();
        mBossLineAttack = GetComponent<BossLineAttack>();
        mBossFollowPlayer = GetComponent<BossFollowPlayer>();
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void Attack()
    {
        BossAttack tBossAttack = GetBossAttack();

        if(tBossAttack == BossAttack.Throw)
        {
           mBossThrow.Throw();
        }
        else if (tBossAttack == BossAttack.LineAttack)
        {
           StartCoroutine(mBossLineAttack.LineAttack());
        }
       /* else if (tBossAttack == BossAttack.Chase)
        {
            StartCoroutine(mBossFollowPlayer.ChasePlayer());
        }*/
    }

    BossAttack GetBossAttack()
    {
        BossAttack tBossAttack = BossAttack.LineAttack;

        int tRandomNumber = Random.Range(0, 3);

        tBossAttack = (BossAttack)tRandomNumber;

        return tBossAttack;
    }

    


}
