﻿using UnityEngine;
using System.Collections;

public class StoneLineRotate : MonoBehaviour
{

    public Transform TransformToMoveTo;

    public float ReachDistance = 0.1f;
    public float Speed;

    private Transform mParent;

    private Vector3 StartRotation;

    private bool mShouldMove;

    private Transform mMyTransform;

	// Use this for initialization
	void Start () {
        Init();
	}

    void Init()
    {
        mMyTransform = transform;
        mParent = mMyTransform.parent;
        StartRotation = mMyTransform.transform.eulerAngles;
    }
	
	// Update is called once per frame
	void Update () {
       

        if (mShouldMove)
        {
            CheckIfReachedPoint();
            MoveToPoint();
        }
	}

    public void Enable()
    {
        mMyTransform.parent = null;
        mMyTransform.eulerAngles = new Vector3(0, 0, 0);
        mShouldMove = true;
    }

    void CheckIfReachedPoint()
    {
        if(HasReachedPoint())
        {
            Disable();
        }
    }

    void Disable()
    {
        mMyTransform.eulerAngles = StartRotation;
        mMyTransform.parent = mParent;
        mShouldMove = false;
        
    }

    void MoveToPoint()
    {
        Vector2 tMoveDirection = (TransformToMoveTo.position - mMyTransform.position).normalized;

        mMyTransform.Translate(tMoveDirection * Speed * Time.deltaTime);
    }

    bool HasReachedPoint()
    {
        bool tReachedPoint = false;

        if (Vector2.Distance(TransformToMoveTo.position, mMyTransform.position) < ReachDistance)
        {
            tReachedPoint = true;
        }

        return tReachedPoint;
    }

    void Reset()
    {
        Disable();
    }
}
