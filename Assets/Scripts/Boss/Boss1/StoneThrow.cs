﻿using UnityEngine;
using System.Collections;

public class StoneThrow : MonoBehaviour {

    public float Speed;

    private bool mThrow;

    private Transform mMyTransform;

	// Use this for initialization
	void Start () {
        mMyTransform = transform;
        
	}
	
	// Update is called once per frame
	void Update () {
        if (mThrow)
        {
            Move();
        }
	}

    public void Throw()
    {
        mThrow = true;
        mMyTransform.parent = null;
    }

    void Move()
    {      

        mMyTransform.Translate(Vector3.up * Speed * Time.deltaTime);
    }

    void Reset()
    {
        mThrow = false;
    }
}
