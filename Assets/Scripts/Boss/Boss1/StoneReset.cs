﻿using UnityEngine;
using System.Collections;

public class StoneReset : MonoBehaviour {

    private Transform mParent;

    private Vector2 mStartPosition;

	// Use this for initialization
	void Start () {
        mParent = transform.parent;
        mStartPosition = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void TheReset()
    {
        GetComponent<Rigidbody2D>().isKinematic = true;
        GetComponent<Rigidbody2D>().gravityScale = 0;
        transform.parent = mParent;
        transform.localPosition = mStartPosition;
        
    }
}
