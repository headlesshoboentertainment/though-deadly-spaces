﻿using UnityEngine;
using System.Collections;

public class BossThrow : MonoBehaviour {

    private Transform mMyTransform;

	// Use this for initialization
	void Start () {
        mMyTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {
        
	}

  

    public void Throw()
    {
        for (int tNumberOfTimesToRunBossThrow = 0; tNumberOfTimesToRunBossThrow < 3; tNumberOfTimesToRunBossThrow++)
        {
            for (int i = 0; i < mMyTransform.childCount; ++i)
            {
                Transform tChild = mMyTransform.GetChild(i);
                if (tChild.tag == "Stone")
                {
                    StoneThrow tStoneThrow = tChild.GetComponent<StoneThrow>();

                    tStoneThrow.Throw();
                }
            }
        }
    
        
   
  
    }
}
