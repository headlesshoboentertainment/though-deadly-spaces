﻿using UnityEngine;
using System.Collections;

public class StoneDamage : MonoBehaviour {

    

    public float AttackSpeed;

    private float mNextTimeToAttack;

    private int mDamage;

    private Boss mBossOne;

	// Use this for initialization
	void Start () {
        mBossOne = transform.parent.GetComponent<Boss>();
        mDamage = mBossOne.Damage;
	}
	
	// Update is called once per frame
	void Update () {
	
	}  

    void OnTriggerEnter2D(Collider2D pCollider)
    {
        if(pCollider.tag == "Player" && CanAttack())
        {
            pCollider.SendMessage("TakeDamage", mDamage);
            mNextTimeToAttack = CalculateNextTimeToAttack();
        }
    }


    bool CanAttack()
    {
        bool tCanAttack = false;

        if (mNextTimeToAttack < Time.time)
        {
            tCanAttack = true;
        }

        return tCanAttack;
    }

    float CalculateNextTimeToAttack()
    {
        float tNextAttack = 0;

        tNextAttack = Time.time + AttackSpeed;

        return tNextAttack;
    }
}
