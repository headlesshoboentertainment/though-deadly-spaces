﻿using UnityEngine;
using System.Collections;

public class BossLineAttack : MonoBehaviour {

    public float RotateSpeedForBoss;

    public int RoundsToGo;

    private BossRotate mBossRotate;

	// Use this for initialization
	void Start () {
        mBossRotate = GetComponent<BossRotate>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public IEnumerator LineAttack()
    {
        for (int tNumberOfTimesToRunBossThrow = 0; tNumberOfTimesToRunBossThrow < 3; tNumberOfTimesToRunBossThrow++)
        {
            foreach (Transform tChild in transform)
            {
                StoneLineRotate tStoneLineRotate = tChild.GetComponent<StoneLineRotate>();
                if (tStoneLineRotate != null)
                {
                    tStoneLineRotate.Enable();
                }
            }
        }

        yield return new WaitForSeconds(1f);
        mBossRotate.Rotate(RoundsToGo, RotateSpeedForBoss);
    }
}
