﻿using UnityEngine;
using System.Collections;

public class BossShield : MonoBehaviour {

    public float Cooldown;
    public float Duration;
    public float TimeToWaitToAttack;

    private BossRotate mBossRotate;
    private BossOne mBossOne;

    private bool mCanAttack;
    
    private float mNextTimeToCanBeUsed;

	// Use this for initialization
	void Start () {
        mBossRotate = GetComponent<BossRotate>();
        mBossOne = GetComponent<BossOne>();
	}
	
	// Update is called once per frame
	void Update () {
	if(!IsOnCooldown())
    {
        ResetStones();
       StartCoroutine(MakeShield());
       StartCoroutine(CallBossToAttack());
    }
	}

    IEnumerator CallBossToAttack()
    {
        yield return new WaitForSeconds(TimeToWaitToAttack);
        mBossOne.SendMessage("Attack");
    }

    public IEnumerator MakeShield()
    {
        mBossRotate.Rotate(0);
        mNextTimeToCanBeUsed = CalculateNextTimeCanBeUsed();
        yield return new WaitForSeconds(Duration);
        mBossRotate.Disable();
        
    }

    

    void ResetStones()
    {
        foreach(GameObject tStone in GameObject.FindGameObjectsWithTag("Stone"))
        {
            tStone.SendMessage("TheReset",SendMessageOptions.DontRequireReceiver);
            tStone.SendMessage("Reset", SendMessageOptions.DontRequireReceiver);
        }
    }

    
    public bool IsOnCooldown()
    {
        bool tIsOnCooldown = true;

        if(mNextTimeToCanBeUsed < Time.time)
        {
            tIsOnCooldown = false;
        }

        return tIsOnCooldown;
    }

    float CalculateNextTimeCanBeUsed()
    {
        float tNextTimeCanBeUsed = 0;

        tNextTimeCanBeUsed = Time.time + Cooldown + Duration;

        return tNextTimeCanBeUsed;
    }
}
