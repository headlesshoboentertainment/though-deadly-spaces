﻿using UnityEngine;
using System.Collections;

public class BossFollowPlayer : MonoBehaviour {

    
    public float TimeToFollowPlayer;

    private bool mMoveToPlayer;

    private float mSpeed;

    private Transform mPlayer;

    private Transform mMyTranform;

    private Boss mBoss;

	// Use this for initialization
	void Start () {
        mBoss = GetComponent<Boss>();
        mSpeed = mBoss.Speed;
        mMyTranform = transform;
	}
	
	// Update is called once per frame
	void Update () {
        if (mMoveToPlayer)
        {
            mPlayer = GameObject.FindGameObjectWithTag("Player").transform;
            MoveToPlayer();
        }
	}

    public IEnumerator ChasePlayer()
    {
        mMoveToPlayer = true;
        yield return new WaitForSeconds(TimeToFollowPlayer);
        mMoveToPlayer = false;
    }

    void MoveToPlayer()
    {
        Vector2 tMoveDirection = (mPlayer.position - mMyTranform.position).normalized;

        mMyTranform.Translate(tMoveDirection * mSpeed * Time.deltaTime);


    }
}
