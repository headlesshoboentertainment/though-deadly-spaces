﻿using UnityEngine;
using System.Collections;

public class MageShot : MonoBehaviour {

    public float Speed;

    public int Damage { get; set; }

    Transform mMyTransform;

	// Use this for initialization
	void Start () {
        mMyTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {
        Move();
	}

    void SetDamage(int pDamage)
    {
        Damage = pDamage;
    }

    void Move()
    {
        mMyTransform.Translate(Vector3.down * Speed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D pCollider)
    {
        if(pCollider.tag == "Player")
        {
            pCollider.SendMessage("TakeDamage", Damage);
            Destroy(gameObject);
        }
    }
}
