﻿using UnityEngine;
using System.Collections;

public class BossRotating : MonoBehaviour {

    private float mRotateSpeed;

    private Boss mBoss;

	// Use this for initialization
	void Start () {
        mBoss = transform.parent.GetComponent<Boss>();
        mRotateSpeed = mBoss.Speed * 10;
	}
	
	// Update is called once per frame
	void Update () {
        Rotate();
	}

    void Rotate()
    {
        transform.Rotate(new Vector3(0, 0, mRotateSpeed * Time.deltaTime));
    }

}
