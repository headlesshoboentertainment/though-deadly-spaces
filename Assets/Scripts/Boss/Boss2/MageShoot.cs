﻿using UnityEngine;
using System.Collections;

public class MageShoot : MonoBehaviour {

    public GameObject Shot;

    private Transform mPlayer;

    private Transform mMyTransform;

    private float mNextTimeToAttack;

    private float mAttackSpeed;

    private int mDamage;

    private Boss mBoss;

    private Animator mAnimator;

	// Use this for initialization
	void Start () {
        mBoss = transform.parent.GetComponent<Boss>();

        mAnimator = GetComponent<Animator>();

        mMyTransform = transform;
        mAttackSpeed = mBoss.AttackSpeed;
        mDamage = mBoss.Damage;
	}
	
	// Update is called once per frame
	void Update () {
        mPlayer = GameObject.FindGameObjectWithTag("Player").transform;
     //   Rotate();
        CheckIfCanShoot();
        PlayIdleAnimation();
	}

    void CheckIfCanShoot()
    {
        if(CanAttack())
        {
            StartCoroutine(PlayShootAnimation());
            Shoot();
            mNextTimeToAttack = CalculateNextTimeToAttack();
        }
    }

    void PlayIdleAnimation()
    {
        if(!mAnimator.GetBool("Shoot"))
        {
            mAnimator.SetBool("Idle", true);
        }
    }

    IEnumerator PlayShootAnimation()
    {
        mAnimator.SetBool("Shoot",true);
        yield return new WaitForSeconds(0.2f);
        mAnimator.SetBool("Shoot", false);
    }

    void Shoot()
    {
        GameObject tShotObject = Instantiate(Shot, mMyTransform.position, Rotate()) as GameObject;
       tShotObject.SendMessage("SetDamage", mDamage);
    }

    

    bool CanAttack()
    {
        bool tCanAttack = false;

        if (mNextTimeToAttack < Time.time)
        {
            tCanAttack = true;
        }

        return tCanAttack;
    }

    float CalculateNextTimeToAttack()
    {
        float tNextAttack = 0;

        tNextAttack = Time.time + mAttackSpeed;

        return tNextAttack;
    }

    Quaternion Rotate()
    {
        Vector3 dir = mPlayer.position - mMyTransform.position;
        float angle = (Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg) + 90;
        return Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
